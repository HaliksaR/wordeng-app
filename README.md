<div align="center">
<a href="https://gitlab.com/HaliksaR/wordeng-app">
<img src="https://user-images.githubusercontent.com/35256960/37650164-b57a8ba4-2c66-11e8-9f42-91fd9a7e63f8.png" width="320" height="320" alt="logo"></img>
</a>
<h1>WordEng</h1>
<h3>Android application</h3>
<p><a href="https://jetbrains.ru/products/kotlin/">Kotlin</a> | 
<a href="https://developer.android.com/jetpack/androidx">androidx</a>  | 
<a href="https://developer.android.com/jetpack/androidx/releases/room">Room</a>  |
<a href="http://jakewharton.github.io/butterknife/">Butterknife</a>  |
<a href="https://airbnb.design/lottie/">Lottie</a>  |
<a href="https://google.github.io/dagger/">Dagger 2</a>
</p>
<h3>Required Android version<h3>
<p>4.1 и выше</p>
<h3>Current version<h3>
<p>Development In Progress -> <a href="https://gitlab.com/HaliksaR/wordeng-app/tree/dev">Pre-Alpha0.0.3</a></p>
<h3>Developer<h3>
<p>HaliksaR</p>
<p><a href="https://haliksar.gitlab.io/site-portfolio/">Web site</a></p>
<p><a href="mailto:fun.haliksar.devel@gmail.com" >fun.haliksar.devel@gmail.com</a></p>
<p><a href="https://vk.com/haliksar">Vk Page</a></p>
</div>